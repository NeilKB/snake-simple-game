#include <SFML/Graphics.hpp>
#include "snake.hpp"
#include <random>
#include <time.h>

#define WIDTH_CASES 32
#define HEIGHT_CASES 18

#define WIDTH_RESOLUTION 800
#define HEIGHT_RESOLUTION 450

#define WIDTH_BLOCK_SIZE 25
#define HEIGHT_BLOCK_SIZE 25

Snake::Snake()
{
    // Create window
    m_window.create(sf::VideoMode(800, 450), "Snake by Neil", sf::Style::Titlebar | sf::Style::Close);
    m_window.setFramerateLimit(60);

    // Load m_block texture (used for the snake and the score)
    sf::Texture m_block_texture;
    if (!m_block_texture.loadFromFile("Sprites/pixel.jpg"))
    {    
            exit(-1);
    }

    m_block.setTexture(m_block_texture, true);
    m_dot.width = WIDTH_BLOCK_SIZE;
    m_dot.height = HEIGHT_BLOCK_SIZE;
}

void Snake::DifficultyScreen()
{
    // Event loop
    bool Continue = true;
    int choice = 0;
    sf::Texture title_texture;
    if (!title_texture.loadFromFile("Sprites/difficulty.jpg"))
    {
        exit(-1);
    }
    sf::Sprite title_sprite;
    title_sprite.setTexture(title_texture, true);
    title_sprite.setPosition(sf::Vector2f(0, 0));

    sf::Texture arrow_texture;
    if (!arrow_texture.loadFromFile("Sprites/arrow.png"))
    {
        exit(-1);
    }
    sf::Sprite arrow;
    arrow.setTexture(arrow_texture, true);

    while (Continue)
    {
        if (m_window.waitEvent(m_event))
        {
            // Evaluate event type
            switch(m_event.type)
            {
                case sf::Event::Closed:
                    Continue = false;
                    m_window.close();
                    break;

                case sf::Event::KeyPressed:
                    // Evaluate keyboard key pressed
                    switch(m_event.key.code)
                    {
                        // Change choice according to the pressed arrow
                        case sf::Keyboard::Up:
                            if (choice > 0)
                            {
                                choice -= 1;
                            }
                            break;
                        case sf::Keyboard::Down:
                            if (choice < 2)
                            {
                                choice += 1;
                            }
                            break;

                        // Selection when enter is pressed
                        case sf::Keyboard::Enter:
                            if (choice == 2)
                            {
                                m_difficulty = 50;
                            }
                            if (choice == 1)
                            {
                                m_difficulty = 100;
                            }
                            if (choice == 0)
                            // When choosing the first option, bring the level selection menu
                            {
                                m_difficulty = 125;
                            }
                            Continue = false;
                            game();
                        default:
                            break;
    

                    }
                default:
                    break;
            }
        }
        arrow.setPosition(sf::Vector2f(0, -215 + choice*105));
        m_window.clear();
        m_window.draw(title_sprite);
        m_window.draw(arrow);
        m_window.display();
    }
}

void Snake::game()
// Game
{
   randomize();
   events(); 
   pause();
}

// Print user score on screen


void Snake::randomize()
{
    std::default_random_engine re(time(0));
    std::uniform_int_distribution<int> plain{0, 0};
    int ini = plain(re); // Initialize randomness
    // Set random x position
    std::uniform_int_distribution<int> distribx{0, WIDTH_CASES-1};
    m_dot.left = distribx(re) * WIDTH_BLOCK_SIZE;

    // Set random y position
    std::uniform_int_distribution<int> distriby{0, HEIGHT_CASES-1};
    m_dot.top = distriby(re) * HEIGHT_BLOCK_SIZE;

}

void Snake::pause()
{
    bool Continue = true;
    while (Continue)
    {
        m_window.waitEvent(m_event);
        if (m_event.type == sf::Event::KeyPressed &&
            m_event.key.code == sf::Keyboard::Enter)
        {
            Continue = false;
        }
    } 


}

void Snake::events()
{
    bool Continue(true);
    sf::String movement(" ");
    while (Continue)
    {
        sf::sleep(sf::milliseconds(m_difficulty));
        while (m_window.pollEvent(m_event))
        {
            switch (m_event.type) 
            {
                case sf::Event::Closed:
                    Continue = false; 
                    break;

                case sf::Event::KeyPressed:
                    // Pause the game
                    if (m_event.key.code == sf::Keyboard::Key::Enter)
                    {
                        pause();
                    }

                    // Move the snake
                    if (m_event.key.code == sf::Keyboard::Key::Up)
                    {
                        movement = sf::String("Up");
                    }

                    if (m_event.key.code == sf::Keyboard::Key::Down)
                    {
                        movement = sf::String("Down");
                    }

                    if (m_event.key.code == sf::Keyboard::Key::Left)
                    {
                        movement = sf::String("Left");
                    }

                    if (m_event.key.code == sf::Keyboard::Key::Right)
                    {
                        movement = sf::String("Right");
                    }

                    break;

                default:
                    break;
                    
            }
        } 
        // Extend the snake size and change m_dot position if the snake eats the m_dot
        if (m_dot.intersects(m_character.get_Block_position(0)))
        {
            m_character.extend();
            randomize();
        }

        Continue = m_character.move(movement);
        if (Continue)
        {
            m_window.clear(sf::Color(0, 122, 0, 0));
            m_character.draw(&m_window, m_block);
            m_block.setPosition(sf::Vector2f(m_dot.left,
                                           m_dot.top));
            m_window.draw(m_block);
        }
        else
        {
            sf::Texture lost_texture;
            sf::Sprite lost;

            if (!lost_texture.loadFromFile("Sprites/lost.png"))
            {    
                    exit(-1);
            }
            lost.setTexture(lost_texture, true);
            m_window.draw(lost);
        }
        m_window.display(); 

    }
}
