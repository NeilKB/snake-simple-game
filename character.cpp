#include <iostream>
#include "character.hpp"
#define MAX_SCORE 50

#define WIDTH_BLOCK_SIZE 25
#define HEIGHT_BLOCK_SIZE 25

#define WIDTH_CASES 32
#define HEIGHT_CASES 18

Character::Character(): block_position{sf::Rect<int>(0, 0, WIDTH_BLOCK_SIZE, HEIGHT_BLOCK_SIZE)}, movement{sf::String(" ")}, m_score(0)
{
}

sf::Rect<int> Character::get_Block_position(int i) const
{
    return block_position[i];
}

int Character::get_Score() const
{
    return m_score;
}


bool Character::move(sf::String &direction)
{
    // If a key was pressed change the snake first block direction
    if (direction == sf::String("Up") && movement[0] != sf::String("Down"))
    {
        movement[0] = sf::String("Up");
    }
    if (direction == sf::String("Left") && movement[0] != sf::String("Right"))
    {
        movement[0] = sf::String("Left");
    }
    if (direction == sf::String("Right") && movement[0] != sf::String("Left"))
    {
        movement[0] = sf::String("Right");
    }
    if (direction == sf::String("Down") && movement[0] != sf::String("Up"))
    {
        movement[0] = sf::String("Down");
    }

    for (int i = 0; i <= m_score; ++i) 
    // Move each block of the character
    {
        if (movement[i] == sf::String("Up"))
        {
            block_position[i].top -= HEIGHT_BLOCK_SIZE; 
        }

        if (movement[i] == sf::String("Down"))
        {
            block_position[i].top += HEIGHT_BLOCK_SIZE; 

        }

        if (movement[i] == sf::String("Left"))
        {
            block_position[i].left -= WIDTH_BLOCK_SIZE; 
        }

        if (movement[i] == sf::String("Right"))
        {
            block_position[i].left += WIDTH_BLOCK_SIZE; 
        }

        // Game over if you quit the rectangle x
        if (block_position[0].left > WIDTH_CASES * WIDTH_BLOCK_SIZE
            || block_position[0].left < 0
            || block_position[0].top > HEIGHT_CASES * HEIGHT_BLOCK_SIZE
            || block_position[0].top < 0)
        {
            return false;
        }
    }

    // The next block take the previous one direction
    for (int i = m_score; i >= 1; --i) 
    {
        movement[i] = movement[i-1];        
    }

    // Set back the direction to none
    direction = sf::String(" ");
    return true;
}

void Character::extend()
{
    block_position[m_score+1].left = block_position[m_score].left;
    block_position[m_score+1].top = block_position[m_score].top;
    ++m_score;
}

void Character::draw(sf::RenderWindow *window, sf::Sprite block) const
{
    // Draw each block of the character in the window
    for (int i = 0; i <= m_score; ++i)
    {
        block.setPosition(sf::Vector2f((block_position[i]).left,
                                       (block_position[i]).top));
        window->draw(block);

    }
}
