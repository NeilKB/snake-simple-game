#include <SFML/Graphics.hpp>
#include "character.hpp"

class Snake
{
    private:
        sf::Event m_event;
        sf::RenderWindow m_window;
        sf::Sprite m_block; // dot sprite
        sf::Rect<int> m_dot; // Give the dot position
        int m_difficulty;
        Character m_character;
        

    public:
        Snake();
        void game();
        void events();
        void randomize();
        void pause();
        void DifficultyScreen();
};
