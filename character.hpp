#include <SFML/Graphics.hpp>
#define MAX_LENGTH 50

class Character
{
    private:
        sf::Rect<int> block_position[MAX_LENGTH];
        sf::String movement[MAX_LENGTH];
        int m_score;

    public:
        Character();
        bool move(sf::String &direction);
        void extend();
        void draw(sf::RenderWindow *window, sf::Sprite block) const;
        int get_Score() const;
        sf::Rect<int> get_Block_position(int i) const;
};
